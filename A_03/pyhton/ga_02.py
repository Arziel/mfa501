"""This module contains a genetic algorithm for evolving a matrix towards a target pattern."""
import random


def generate_binary_matrix(size):
    """ Generates a size x size matrix filled with 0's and 1's. """
    return [[random.choice([0, 1]) for _ in range(size)] for _ in range(size)]


# Generate a 10x10 binary matrix
binary_matrix = generate_binary_matrix(10)


def fitness(matrix, target):
    """ Calculate the fitness score of a matrix compared to the target matrix. """
    score = 0
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if matrix[i][j] == target[i][j]:
                score += 1
    return score


def crossover(parent1, parent2):
    """ Perform crossover between two parent matrices. """
    child = []
    for i in range(len(parent1)):
        if random.choice([True, False]):
            child.append(parent1[i])
        else:
            child.append(parent2[i])
    return child


def mutate(matrix, mutation_rate):
    """ Randomly mutate a matrix based on a mutation rate. """
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if random.random() < mutation_rate:
                matrix[i][j] = 1 if matrix[i][j] == 0 else 0


def genetic_algorithm(target, population_size, mutation_rate, generations):
    """ Run the genetic algorithm. """
    # Generate initial population
    population = [generate_binary_matrix(
        len(target)) for _ in range(population_size)]
    fitness_scores = []  # Initialize the variable to avoid unbound errors

    for _ in range(generations):  # Replace 'generation' with '_' if it's not used
        # Evaluate fitness
        fitness_scores = [fitness(matrix, target) for matrix in population]

        # Check for a solution
        if max(fitness_scores) == len(target) * len(target[0]):
            return population[fitness_scores.index(max(fitness_scores))]

        # Selection
        sorted_population = [x for _, x in sorted(
            zip(fitness_scores, population), key=lambda pair: pair[0], reverse=True)]
        population = sorted_population[:int(
            population_size / 2)]  # keep top 50%

        # Crossover
        children = []
        while len(children) < population_size / 2:
            parent1, parent2 = random.sample(population, 2)
            child = crossover(parent1, parent2)
            children.append(child)

        # Mutation
        for child in children:
            mutate(child, mutation_rate)

        # Create new population
        population.extend(children)

    # If no exact solution was found, return the best one
    best_score_index = fitness_scores.index(max(fitness_scores))
    return population[best_score_index]


# Define the target smiley face matrix
target_smiley = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 1, 0, 1, 0, 0, 1, 0, 1, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 1, 0, 1, 0, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 1, 1, 1, 0, 1, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
]

# Run the genetic algorithm
solution = genetic_algorithm(
    target_smiley, population_size=10000, mutation_rate=0.01, generations=10000)


def print_matrix(matrix):
    """ Prints the matrix in a more readable format using unicode characters. """
    for row in matrix:
        print(' '.join(['\u2B1B' if x == 0 else '\u2B1C' for x in row]))
    print("\n")


# Print the original random matrix
print("Original matrix:")
print_matrix(binary_matrix)

# Print the solution in a more readable format
print("Solution:")
print_matrix(solution)
