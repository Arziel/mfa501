"""This module contains a genetic algorithm for evolving
a matrix towards a target pattern using a grid search."""
import random


def generate_binary_matrix(size):
    """Generates a size x size matrix filled with 0's and 1's."""
    return [[random.choice([0, 1]) for _ in range(size)] for _ in range(size)]


def fitness(matrix, target):
    """Calculates the fitness score of a matrix compared to the target matrix."""
    return sum(matrix[i][j] == target[i][j] for i in range(len(matrix)) for j in range(len(matrix[i])))


def crossover(parent1, parent2):
    """Performs crossover between two parent matrices."""
    return [random.choice([p1, p2]) for p1, p2 in zip(parent1, parent2)]


def mutate(matrix, mutation_rate):
    """Randomly mutate a matrix based on a mutation rate."""
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            if random.random() < mutation_rate:
                matrix[i][j] = 1 if matrix[i][j] == 0 else 0


def genetic_algorithm(target, population_size, mutation_rate, generations):
    """Runs the genetic algorithm."""
    population = [generate_binary_matrix(
        len(target)) for _ in range(population_size)]
    fitness_scores = [0] * population_size

    for _ in range(generations):
        fitness_scores = [fitness(matrix, target) for matrix in population]
        if max(fitness_scores) == len(target) * len(target[0]):
            return population[fitness_scores.index(max(fitness_scores))]
        sorted_population = [x for _, x in sorted(
            zip(fitness_scores, population), key=lambda pair: pair[0], reverse=True)]
        population = sorted_population[:int(population_size / 2)]
        children = []
        while len(children) < population_size / 2:
            parent1, parent2 = random.sample(population, 2)
            child = crossover(parent1, parent2)
            mutate(child, mutation_rate)
            children.append(child)
        population.extend(children)

    return population[fitness_scores.index(max(fitness_scores))]


def print_matrix(matrix):
    """Prints the matrix in a more readable format using unicode characters."""
    for row in matrix:
        print(' '.join(['\u2B1B' if x == 0 else '\u2B1C' for x in row]))
    print("\n")


def grid_search(target, population_sizes, mutation_rates, generation_counts):
    """Performs a grid search to find the best hyperparameters for the genetic algorithm."""
    best_parameters = None
    best_fitness = -1
    best_solution = None

    for pop_size in population_sizes:
        for mut_rate in mutation_rates:
            for gen_count in generation_counts:
                current_solution = genetic_algorithm(
                    target, pop_size, mut_rate, gen_count)
                current_fitness = fitness(current_solution, target)

                if current_fitness > best_fitness:
                    best_fitness = current_fitness
                    best_parameters = (pop_size, mut_rate, gen_count)
                    best_solution = current_solution

    return best_parameters, best_fitness, best_solution


# Define the target smiley face matrix
target_smiley = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 1, 0, 1, 0, 0, 1, 0, 1, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 1, 0, 1, 0, 0, 1, 0, 1, 0],
    [0, 1, 0, 1, 1, 1, 1, 0, 1, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
]

# Define the ranges for grid search parameters
population_sizes = [100, 500]  # , 500, 1000]
mutation_rates = [0.001, 0.01]  # , 0.01, 0.1]
generation_counts = [100, 500]  # , 500, 1000]

# Perform the grid search
best_params, best_fit, best_sol = grid_search(
    target_smiley, population_sizes, mutation_rates, generation_counts)

# Print out the best parameters and the fitness of the best solution
if best_params is not None:
    print("Best Parameters:")
    print(f"- Population Size={best_params[0]}")
    print(f"- Mutation Rate={best_params[1]}")
    print(f"- Generations={best_params[2]}")
else:
    print("No best parameters found.")
print(f"Best Fitness: {best_fit}")

print("Best Solution Matrix:")
print_matrix(best_sol)
