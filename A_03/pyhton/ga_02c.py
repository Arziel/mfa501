# Import necessary libraries
import random
import multiprocessing
from functools import partial


def generate_binary_matrix(size):
    """
    Generates a binary matrix of given size with random 0s and 1s.

    Parameters:
    size (int): The size of the matrix to generate.

    Returns:
    list: A 2D list representing the binary matrix.
    """
    return [[random.randint(0, 1) for _ in range(size)] for _ in range(size)]


def fitness(matrix, target):
    """
    Calculates the fitness of a matrix compared to a target matrix.

    Parameters:
    matrix (list): The matrix to calculate fitness for.
    target (list): The target matrix to compare against.

    Returns:
    int: The fitness score of the matrix.
    """
    return sum(matrix[i][j] == target[i][j] for i, row in enumerate(matrix) for j, _ in enumerate(row))

# This function is used to calculate fitness in parallel


def calculate_fitness(matrix, target):
    """
    Calculates the fitness of a given matrix with respect to a target.

    Args:
        matrix (list): The matrix to calculate the fitness of.
        target (list): The target matrix to compare against.

    Returns:
        float: The fitness score of the matrix.
    """
    return fitness(matrix, target)


def crossover(parent1, parent2):
    """
    Performs crossover between two parent matrices.

    Parameters:
    parent1 (list): The first parent matrix.
    parent2 (list): The second parent matrix.

    Returns:
    list: The child matrix after crossover.
    """
    return [parent1[i] if random.getrandbits(1) else parent2[i] for i in range(len(parent1))]


def mutate(matrix, mutation_rate):
    """
    Performs mutation on a matrix with a given mutation rate.

    Parameters:
    matrix (list): The matrix to mutate.
    mutation_rate (float): The rate at which to mutate the matrix.
    """
    for i, row in enumerate(matrix):
        for j, _ in enumerate(row):
            if random.random() < mutation_rate:
                matrix[i][j] = 1 - matrix[i][j]


def genetic_algorithm(target, population_size, mutation_rate, generations):
    """
    Performs a genetic algorithm to evolve a population of matrices towards a target matrix.

    Parameters:
    target (list): The target matrix to evolve towards.
    population_size (int): The size of the population.
    mutation_rate (float): The rate at which to mutate the matrices.
    generations (int): The number of generations to evolve over.

    Returns:
    list: The matrix from the final generation with the highest fitness.
    """
    population = [generate_binary_matrix(
        len(target)) for _ in range(population_size)]
    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    for _ in range(generations):
        fitness_scores = pool.map(
            partial(calculate_fitness, target=target), population)
        if max_score := max(fitness_scores) == len(target)**2:
            return population[fitness_scores.index(max_score)]

        # Sort the population based on fitness scores in descending order
        population = [x for _, x in sorted(
            zip(fitness_scores, population), key=lambda pair: pair[0], reverse=True)]

        # Keep the top 50% of the population
        elites = population[:population_size//2]

        # Select parents from the elites
        parents = random.choices(
            elites, weights=fitness_scores[:population_size//2], k=population_size//2)

        # Perform crossover and mutation to generate children
        children = [crossover(*random.sample(parents, 2))
                    for _ in range(population_size//2)]
        for child in children:
            mutate(child, mutation_rate)

        # Update the population with the elites and children
        population = elites + children

    return max(population, key=lambda matrix: fitness(matrix, target))


def print_matrix(matrix):
    """
    Prints a matrix with 0s represented as ⬛ and 1s represented as ⬜.

    Parameters:
    matrix (list): The matrix to print.
    """
    for row in matrix:
        print(' '.join('\u2B1B' if x == 0 else '\u2B1C' for x in row))
    print("\n")


binary_matrix = generate_binary_matrix(10)
target_smiley = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 1, 0, 0, 0, 0, 0, 0, 1, 0], [0, 1, 0, 1, 0, 0, 1, 0, 1, 0], [0, 1, 0, 0, 0, 0, 0, 0, 1, 0], [
    0, 1, 0, 1, 0, 0, 1, 0, 1, 0], [0, 1, 0, 1, 1, 1, 1, 0, 1, 0], [0, 1, 0, 0, 0, 0, 0, 0, 1, 0], [0, 1, 1, 1, 1, 1, 1, 1, 1, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
solution = genetic_algorithm(
    target_smiley, population_size=1, mutation_rate=0.01, generations=2)

print("Original matrix:")
print_matrix(binary_matrix)
print("Solution:")
print_matrix(solution)
