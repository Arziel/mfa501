"""
This module implements a genetic algorithm to evolve a
population of binary matrices towards a target binary matrix.
"""

import random

# Global array to store top matrices at each interval
top_matrices = []


def generate_binary_matrix(matrix_size):
    """Generates a binary matrix of a given size with random 0s and 1s."""
    return bytearray(random.getrandbits(1) for _ in range(matrix_size * matrix_size))


def fitness(individual_matrix, target, matrix_size):
    """Calculates the fitness of a matrix compared to a target matrix."""
    return sum(individual_matrix[i] == target[i] for i in range(matrix_size * matrix_size))


def crossover(parent1, parent2, matrix_size):
    """Performs crossover between two parent matrices."""
    crossover_point = random.randint(0, matrix_size * matrix_size)
    child = bytearray(matrix_size * matrix_size)
    child[:crossover_point] = parent1[:crossover_point]
    child[crossover_point:] = parent2[crossover_point:]
    return child


def mutate(individual_matrix, mutation_rate, matrix_size):
    """Performs mutation on a matrix with a given mutation rate."""
    num_mutations = int(matrix_size * matrix_size * mutation_rate)
    for _ in range(num_mutations):
        i = random.randrange(matrix_size * matrix_size)
        individual_matrix[i] ^= 1


def genetic_algorithm(target, population_size, mutation_rate, generations, matrix_size):
    """Performs a genetic algorithm to evolve a population of matrices towards a target matrix."""
    global top_matrices
    top_matrices = []  # Reset for each run
    population = [generate_binary_matrix(
        matrix_size) for _ in range(population_size)]
    target_flat = bytearray(target)

    for gen in range(generations):
        # Store the top matrix at each 10% interval, including generation 0
        current_top_matrix = max(population, key=lambda matrix: fitness(
            matrix, target_flat, matrix_size))
        if gen % (generations // 10) == 0 or gen == 0:
            top_matrices.append((gen, current_top_matrix))
        elites_count = int(population_size * 0.1)
        elites = sorted(population, key=lambda matrix: fitness(
            matrix, target_flat, matrix_size), reverse=True)[:elites_count]
        selection_pool = population + elites
        population = random.choices(
            selection_pool, k=population_size - elites_count)
        children = [crossover(*random.sample(population, 2), matrix_size)
                    for _ in range(population_size - elites_count)]
        for child in children:
            mutate(child, mutation_rate, matrix_size)
        population.extend(children)
        population = population[:population_size]
        mutation_rate *= (1 - (gen / generations))

    return max(population, key=lambda matrix: fitness(matrix, target_flat, matrix_size))


def print_matrix(matrix_data, matrix_size):
    """Prints a matrix with 0s represented as ⬛ and 1s represented as ⬜."""
    for i in range(matrix_size):
        print(' '.join('\u2B1B' if matrix_data[i * matrix_size + j]
              == 0 else '\u2B1C' for j in range(matrix_size)))
    print("\n")


# Constants and genetic algorithm execution
MATRIX_SIZE = 10
POPULATION_SIZE = 1000
MUTATION_RATE = 0.05
GENERATIONS = 10000
target_smiley = bytearray([
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 1, 0, 1, 0, 0, 1, 0, 1, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 1, 0, 1, 0, 0, 1, 0, 1, 0,
    0, 1, 0, 1, 1, 1, 1, 0, 1, 0,
    0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0
])  # Define the target smiley matrix

solution = genetic_algorithm(
    target_smiley, POPULATION_SIZE, MUTATION_RATE, GENERATIONS, MATRIX_SIZE)

# Print the top matrices at each interval and the solution
for generation, matrix in top_matrices:
    print(f"Top matrix at generation {generation}:")
    print_matrix(matrix, MATRIX_SIZE)

# Print the final solution
print("Final Solution:")
print_matrix(solution, MATRIX_SIZE)

# Print the used values
print("Used Values:")
print(f"Population Size: {POPULATION_SIZE}")
print(f"Mutation Rate: {MUTATION_RATE}")
print(f"Generations: {GENERATIONS}")
print(f"Matrix Size: {MATRIX_SIZE}")
