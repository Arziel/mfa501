let currentInfoButton = null;

document.querySelectorAll('.info-btn').forEach(function (button) {
    button.addEventListener('click', function (event) {
        let infoId = 'info-details-' + event.target.id.split('-')[2];
        let infoDetails = document.getElementById(infoId);
        if (currentInfoButton && currentInfoButton !== button) {
            currentInfoButton.classList.remove('info-btn-highlighted');
            currentInfoDetails.style.display = 'none';
        }
        if (
            infoDetails.style.display === 'none' ||
            infoDetails.style.display === ''
        ) {
            infoDetails.style.display = 'block';
            button.classList.add('info-btn-highlighted');
        } else {
            infoDetails.style.display = 'none';
            button.classList.remove('info-btn-highlighted');
        }
        currentInfoDetails = infoDetails;
        currentInfoButton = button;
    });
});
