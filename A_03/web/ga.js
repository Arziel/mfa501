// Define the size of the matrix
const MATRIX_SIZE = 10;

// Get the start button element from the DOM
const startButton = document.getElementById('startButton');

// Get the input elements from the DOM
const timeoutInput = document.getElementById('algorithmTimeout');
const populationSizeInput = document.getElementById('populationSize');
const elitePercentageInput = document.getElementById('elitePercentage');
const mutationRateInput = document.getElementById('mutationRate');
const generationsInput = document.getElementById('generations');

// Initialize variables
let isAlgorithmRunning = false;
let algorithmTimeout;

/**
 * Generates a binary matrix of a given size.
 *
 * @param {number} size - The size of the matrix.
 * @returns {number[]} - The generated binary matrix.
 */
function generateBinaryMatrix(size) {
    return Array.from({ length: size * size }, () => Math.round(Math.random()));
}

/**
 * Calculates the fitness score of a matrix based on a target matrix.
 *
 * @param {number[]} matrix - The matrix to calculate the fitness score for.
 * @param {number[]} target - The target matrix to compare against.
 * @returns {number} - The fitness score of the matrix.
 */
function fitness(matrix, target) {
    return matrix.reduce(
        (score, cell, i) => score + (cell === target[i] ? 1 : 0),
        0
    );
}

/**
 * Performs crossover between two parent matrices at a given crossover point.
 *
 * @param {number[]} parent1 - The first parent matrix.
 * @param {number[]} parent2 - The second parent matrix.
 * @param {number} crossoverPoint - The crossover point index.
 * @returns {number[]} - The resulting matrix after crossover.
 */
function crossover(parent1, parent2, crossoverPoint) {
    return parent1
        .slice(0, crossoverPoint)
        .concat(parent2.slice(crossoverPoint));
}

/**
 * Performs mutation on a matrix based on a given mutation rate.
 *
 * @param {number[]} matrix - The matrix to mutate.
 * @param {number} mutationRate - The mutation rate.
 * @returns {number[]} - The mutated matrix.
 */
function mutate(matrix, mutationRate) {
    return matrix.map((cell) =>
        Math.random() < mutationRate ? 1 - cell : cell
    );
}

/**
 * Runs the genetic algorithm to evolve a population of matrices towards a target matrix.
 *
 * @param {number[]} target - The target matrix to evolve towards.
 * @param {number} timeoutMs - The timeout of the algorithm (speed at which we see it change on screen).
 * @param {number} populationSize - The size of the population.
 * @param {number} mutationRate - The mutation rate.
 * @param {number} generations - The number of generations to run the algorithm.
 * @param {number} matrixSize - The size of the matrices.
 */
function geneticAlgorithm(
    target,
    timeoutMs,
    populationSize,
    elitePercentage,
    mutationRate,
    generations,
    matrixSize
) {
    // Generate an initial population of matrices
    let population = Array(populationSize)
        .fill()
        .map(() => generateBinaryMatrix(matrixSize));

    // Flatten the target matrix
    let targetFlat = target.flat();

    // Create a cache to store fitness scores
    let fitnessCache = new Map();

    /**
     * Calculates the fitness score of a matrix and caches the result.
     *
     * @param {number[]} matrix - The matrix to calculate the fitness score for.
     * @returns {number} - The fitness score of the matrix.
     */
    const calculateFitness = (matrix) => {
        const key = matrix.join('');
        if (!fitnessCache.has(key)) {
            fitnessCache.set(key, fitness(matrix, targetFlat));
        }
        return fitnessCache.get(key);
    };

    /**
     * Runs a single generation of the genetic algorithm.
     *
     * @param {number} gen - The current generation number.
     */
    const runGeneration = (gen) => {
        // Find the matrix with the highest fitness score in the current population
        let currentTopMatrix = population.reduce((max, curr) =>
            calculateFitness(curr) > calculateFitness(max) ? curr : max
        );

        // Update the UI grid with the current top matrix
        updateGrid(currentTopMatrix, matrixSize);

        // Update the current generation display
        document.getElementById(
            'currentGeneration'
        ).innerText = `Current Generation: ${gen}`;

        // If the current top matrix is the target matrix, stop the algorithm
        if (calculateFitness(currentTopMatrix) === matrixSize * matrixSize) {
            isAlgorithmRunning = false;
            document.getElementById('startButton').innerText = 'Start';
            return;
        }

        // Select the top X% of the population as elites
        let elitesCount = Math.floor((elitePercentage / 100) * populationSize);
        let elites = population
            .slice()
            .sort((a, b) => calculateFitness(b) - calculateFitness(a))
            .slice(0, elitesCount);

        // Create the next generation by performing crossover and mutation
        let nextGeneration = elites.slice();
        while (nextGeneration.length < populationSize) {
            let parent1 =
                population[Math.floor(Math.random() * populationSize)];
            let parent2 =
                population[Math.floor(Math.random() * populationSize)];
            let crossoverPoint = Math.floor(
                Math.random() * matrixSize * matrixSize
            );
            let child = crossover(parent1, parent2, crossoverPoint);
            child = mutate(child, mutationRate);
            nextGeneration.push(child);
        }

        // Update the population and mutation rate for the next generation
        population = nextGeneration;
        mutationRate *= 1 - gen / generations;

        // If there are more generations to run and the algorithm is still running, continue to the next generation
        if (gen < generations - 1 && isAlgorithmRunning) {
            algorithmTimeout = setTimeout(
                () => runGeneration(gen + 1),
                timeoutMs
            );
        } else {
            // Stop the algorithm and update the start button text
            isAlgorithmRunning = false;
            document.getElementById('startButton').innerText = 'Start';
        }
    };

    // Start the algorithm with the first generation
    runGeneration(0);
}

/**
 * The target smiley matrix.
 *
 * @type {number[]}
 */
const targetSmiley = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0,
    0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
];

/**
 * Updates the UI grid based on the matrix.
 *
 * @param {number[]} matrix - The matrix to update the UI grid with.
 */
function updateGrid(matrix) {
    let grid = document.getElementById('grid');
    grid.innerHTML = matrix
        .map((cell) => `<div class="${cell === 1 ? 'on' : 'off'}"></div>`)
        .join('');
}

/**
 * Event listener for the start button click.
 */
startButton.addEventListener('click', () => {
    if (isAlgorithmRunning) {
        // If the algorithm is running, stop it
        clearTimeout(algorithmTimeout);
        isAlgorithmRunning = false;
    } else {
        // If the algorithm is not running, start it
        const timeoutMs = parseInt(timeoutInput.value);
        const populationSize = parseInt(populationSizeInput.value, 10);
        const elitePercentage = parseFloat(elitePercentageInput.value);
        const mutationRate = parseFloat(mutationRateInput.value);
        const generations = parseInt(generationsInput.value, 10);
        isAlgorithmRunning = true;
        geneticAlgorithm(
            targetSmiley,
            timeoutMs,
            populationSize,
            elitePercentage,
            mutationRate,
            generations,
            MATRIX_SIZE
        );
    }
    // Update the start button text
    startButton.innerText = isAlgorithmRunning ? 'Stop' : 'Start';
});
