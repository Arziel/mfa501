/**
 * Generates a random matrix of size n x n.
 * @param {number} n - The size of the matrix.
 * @returns {number[][]} - The generated matrix.
 */
function generateRandomMatrix(n) {
    return Array.from({ length: n }, () =>
        Array.from({ length: n }, () => Math.floor(Math.random() * 10))
    );
}

/**
 * Displays the matrix on the webpage.
 * @param {number[][]} matrix - The matrix to display.
 */
function displayMatrix(matrix) {
    const matrixOutputElement = document.getElementById('matrixOutput');
    matrixOutputElement.style.gridTemplateColumns = `repeat(${matrix.length}, 1fr)`;
    const matrixHTML = matrix
        .map((row) => row.map((value) => `<div>${value}</div>`).join(''))
        .join('');
    matrixOutputElement.innerHTML = matrixHTML;
}

/**
 * Calculates the determinant of a matrix using recursion.
 * @param {number[][]} matrix - The matrix to calculate the determinant for.
 * @returns {number} - The determinant of the matrix.
 */
function determinant(matrix) {
    const n = matrix.length;
    let det = 0;

    // Base case: If the matrix is 1x1, return the single element as the determinant
    if (n === 1) {
        return matrix[0][0];
    }

    // Base case: If the matrix is 2x2, calculate the determinant using the formula
    if (n === 2) {
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
    }

    // Recursive case: Calculate the determinant using cofactor expansion along the first row
    for (let i = 0; i < n; i++) {
        // Calculate the minor matrix by removing the first row and the i-th column
        let minor = matrix.slice(1).map((row) => row.filter((_, j) => j !== i));

        // Calculate the cofactor by multiplying the element with the sign (-1)^i
        let cofactor = matrix[0][i] * Math.pow(-1, i);

        // Recursively calculate the determinant of the minor matrix
        let minorDeterminant = determinant(minor);

        // Add the product of the cofactor and the determinant of the minor matrix to the total determinant
        det += cofactor * minorDeterminant;
    }

    // Return the calculated determinant
    return det;
}

// Event listener for the "Generate" button
document.getElementById('generateMatrix').addEventListener('click', () => {
    const n = parseInt(document.getElementById('matrixSize').value, 10);
    const matrix = generateRandomMatrix(n);
    displayMatrix(matrix);
    const det = determinant(matrix);
    document.getElementById(
        'determinantOutput'
    ).textContent = `Determinant: ${det}`;
});
