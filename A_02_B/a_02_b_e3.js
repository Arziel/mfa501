/**
 * Map to cache the calculated values of the sinc function.
 * Key: x value, Value: sinc(x)
 */
const sincCache = new Map();

/**
 * Calculates the sinc function for a given x value.
 * @param {number} x - The input value.
 * @returns {number} - The calculated sinc value.
 */
function sinc(x) {
    if (!sincCache.has(x)) {
        // Calculate sinc(x) and store it in the cache if not already calculated
        sincCache.set(x, x !== 0 ? Math.sin(x) / x : 1);
    }
    return sincCache.get(x);
}

/**
 * Calculates the value of f1(x) = sinc(x).
 * @param {number} x - The input value.
 * @returns {number} - The calculated value of f1(x).
 */
function f1(x) {
    return sinc(x);
}

/**
 * Calculates the value of f2(x, y) = sinc(x) * sinc(y).
 * @param {number} x - The input value for x.
 * @param {number} y - The input value for y.
 * @returns {number} - The calculated value of f2(x, y).
 */
function f2(x, y) {
    return sinc(x) * sinc(y);
}

// Event listeners for the X and Y sliders
document.getElementById('sliderX').addEventListener('input', function () {
    updateFunctionValues();
});

document.getElementById('sliderY').addEventListener('input', function () {
    updateFunctionValues();
});

/**
 * Updates the displayed function values based on the X and Y slider values.
 */
function updateFunctionValues() {
    const x = parseFloat(document.getElementById('sliderX').value);
    const y = parseFloat(document.getElementById('sliderY').value);

    // Update the displayed values
    document.getElementById('outputX').textContent = x.toFixed(2);
    document.getElementById('outputY').textContent = y.toFixed(2);
    document.getElementById('f1Value').textContent = f1(x).toFixed(4);
    document.getElementById('f2Value').textContent = f2(x, y).toFixed(4);
}

/**
 * Plots the functions f1(x) and f2(x, x) using Chart.js library.
 */
function plotFunctions() {
    const ctx = document.getElementById('functionChart').getContext('2d');
    const dataPoints1 = [];
    const dataPoints2 = [];

    // Generate data points for x in the range [-1, 1] with a step of 0.1
    for (let x = -1; x <= 1; x += 0.1) {
        dataPoints1.push({ x: x, y: f1(x) });
        dataPoints2.push({ x: x, y: f2(x, x) }); // Using f2(x, x) for simplicity
    }

    // Create a line chart using Chart.js
    new Chart(ctx, {
        type: 'line',
        data: {
            datasets: [
                {
                    label: 'f1 = sin(x)/x',
                    data: dataPoints1,
                    borderColor: 'red',
                    fill: false,
                },
                {
                    label: 'f2 = (sin(x)/x) * (sin(y)/y)',
                    data: dataPoints2,
                    borderColor: 'blue',
                    fill: false,
                },
            ],
        },
        options: {
            scales: {
                x: { type: 'linear', position: 'bottom' },
                y: { beginAtZero: true },
            },
        },
    });
}

/**
 * Plots the functions f1(x) and f2(x, y) in a 3D plot using Plotly library.
 */
function plot3DFunctions() {
    let trace1 = { x: [], y: [], z: [], type: 'mesh3d', name: 'f1 = sin(x)/x' };
    let trace2 = {
        x: [],
        y: [],
        z: [],
        type: 'mesh3d',
        name: 'f2 = (sin(x)/x) * (sin(y)/y)',
    };

    // Generate values for x and y in the range [-1, 1] with a step of 0.1
    for (let x = -1; x <= 1; x += 0.1) {
        for (let y = -1; y <= 1; y += 0.1) {
            trace1.x.push(x);
            trace1.y.push(y);
            trace1.z.push(f1(x));
            trace2.x.push(x);
            trace2.y.push(y);
            trace2.z.push(f2(x, y));
        }
    }

    let layout = {
        title: '3D Visualization of Functions',
        autosize: true,
        margin: { l: 10, r: 10, b: 10, t: 30 },
    };

    // Create a 3D plot using Plotly
    Plotly.newPlot('function3DPlot', [trace1, trace2], layout);
}

/**
 * Displays the calculated values of f1(x) and f2(x, y) in a scrollable list.
 */
function displayFunctionValues() {
    let outputElement = document.getElementById('scrollableList');
    let outputHTML = [];

    // Generate values for x and y in the range [-1, 1] with a step of 0.1
    for (let x = -1; x <= 1; x += 0.1) {
        for (let y = -1; y <= 1; y += 0.1) {
            outputHTML.push(
                `f1(${x.toFixed(1)}) = ${f1(x).toFixed(4)}, f2(${x.toFixed(
                    1
                )}, ${y.toFixed(1)}) = ${f2(x, y).toFixed(4)}<br>`
            );
        }
    }

    // Display the generated HTML in the scrollable list element
    outputElement.innerHTML = outputHTML.join('');
}

// Call the necessary functions to initialize the program
plotFunctions();
plot3DFunctions();
displayFunctionValues();
updateFunctionValues();
