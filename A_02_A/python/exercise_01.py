"""
Eigenvalue Calculation using the Bisection Method

This module implements the Bisection Method to find an eigenvalue of a 2x2 matrix. 
The algorithm iteratively narrows down an interval where the eigenvalue is located until 
a given tolerance level is reached. Parameters such as the matrix, tolerance, and initial
interval can be easily adjusted within the script.

Developed using Python 3.11.6, compatible with Python 3.x.
"""

import random


def determinant(matrix_2x2):
    """
    Calculate the determinant of a 2x2 matrix.
    """
    return matrix_2x2[0][0] * matrix_2x2[1][1] - matrix_2x2[0][1] * matrix_2x2[1][0]


def bisection_eigenvalues(matrix_2x2, lower_bound, upper_bound, tolerance):
    """
    Find an eigenvalue of a 2x2 matrix using the Bisection Method.

    Parameters:
    - matrix_2x2: The 2x2 matrix for which an eigenvalue is to be found.
    - lower_bound: The lower bound for the eigenvalue.
    - upper_bound: The upper bound for the eigenvalue.
    - tolerance: The error tolerance for the eigenvalue calculation.

    Returns:
    - eigenvalue: The calculated eigenvalue within the given tolerance.
    """
    # Initialize eigenvalue to zero
    eigenvalue = 0.0

    # Loop until the error is less than the tolerance
    while upper_bound - lower_bound > tolerance:
        # Calculate the midpoint of the current interval
        midpoint = (upper_bound + lower_bound) / 2.0

        # Create a matrix where the diagonal entries are shifted by the negative midpoint
        shifted_matrix = [[matrix_2x2[i][j] - midpoint if i ==
                           j else matrix_2x2[i][j] for j in range(2)] for i in range(2)]

        # Calculate the determinant of the shifted matrix
        det_shifted_matrix = determinant(shifted_matrix)

        # Check if the determinant is zero (an eigenvalue is found)
        if det_shifted_matrix == 0:
            eigenvalue = midpoint
            break
        # Update bounds based on the determinant
        if det_shifted_matrix > 0:
            lower_bound = midpoint
        else:
            upper_bound = midpoint

    # If no eigenvalue found during the loop, take the average of the final bounds
    if eigenvalue == 0:
        eigenvalue = (upper_bound + lower_bound) / 2.0

    return eigenvalue


# Example usage
example_matrix_2x2 = [[4, 2], [3, 3]]
CALCULATED_EIGENVALUE = bisection_eigenvalues(
    example_matrix_2x2, -10, 10, 1e-5)

print("Example matrix 2x2:\n" +
      '\n'.join([''.join(['{:4}'.format(item) for item in row]) for row in example_matrix_2x2]))
print("Calculated eigenvalue: ", CALCULATED_EIGENVALUE)


# Test the Bisection Method with a 5x5 matrix
# Since the Bisection Method is implemented for 2x2 matrices, we'll extract a 2x2 submatrix from the 5x5 matrix

# Generate a random 5x5 matrix with integer values between -10 and 10
random_matrix_5x5 = [[random.randint(-10, 10)
                      for _ in range(5)] for _ in range(5)]

# Extract the top-left 2x2 submatrix from the random 5x5 matrix
top_left_2x2_from_5x5 = [[random_matrix_5x5[i][j]
                          for j in range(2)] for i in range(2)]

# Use the bisection method to find an eigenvalue of the top-left 2x2 submatrix
EIGENVALUE_BISECTION_FROM_5X5 = bisection_eigenvalues(
    top_left_2x2_from_5x5, -20, 20, 1e-5)

# Print the results
print("Random 5x5 matrix:\n" +
      '\n'.join([''.join(['{:4}'.format(item) for item in row]) for row in random_matrix_5x5]))
print("Top-left 2x2 submatrix:\n" +
      '\n'.join([''.join(['{:4}'.format(item) for item in row]) for row in top_left_2x2_from_5x5]))
print("Calculated eigenvalue: ", EIGENVALUE_BISECTION_FROM_5X5)
