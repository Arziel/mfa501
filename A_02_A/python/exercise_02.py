"""
Integral Calculation using the Trapezoidal Rule

This module uses the Trapezoidal Rule to approximate the integral of a given function 
over a specified interval. The function to be integrated and the interval can be set 
within the script. The number of trapezoids used for approximation can also be adjusted 
for accuracy.

Developed using Python 3.11.6, compatible with Python 3.x.
"""


def function_to_integrate(x):
    """
    Define the function to be integrated.
    """
    return x ** 2


def trapezoidal_rule_integral(func, lower_limit, upper_limit, num_intervals):
    """
    Calculate the integral of a function using the Trapezoidal Rule.

    Parameters:
    - func: The function to be integrated.
    - lower_limit: The lower limit of integration.
    - upper_limit: The upper limit of integration.
    - num_intervals: The number of trapezoids (intervals).

    Returns:
    - integral_value: The approximate integral value.
    """
    # Calculate the width of each trapezoid
    interval_width = (upper_limit - lower_limit) / num_intervals

    # Initialize the integral value to zero
    integral_value = 0.0

    # Loop through each interval to sum up trapezoid areas
    for i in range(num_intervals):
        # Calculate the x-values for the left and right endpoints of the current trapezoid
        x_left = lower_limit + i * interval_width
        x_right = lower_limit + (i + 1) * interval_width

        # Calculate the area of the current trapezoid and add it to the integral value
        trapezoid_area = (func(x_left) + func(x_right)) * interval_width / 2
        integral_value += trapezoid_area

    return integral_value


# Example usage
LOWER_LIMIT_EXAMPLE = 0
UPPER_LIMIT_EXAMPLE = 1
NUM_INTERVALS_EXAMPLE = 1000
calculated_integral = trapezoidal_rule_integral(
    function_to_integrate, LOWER_LIMIT_EXAMPLE, UPPER_LIMIT_EXAMPLE, NUM_INTERVALS_EXAMPLE)

# Print the calculated integral
print("Lower limit: ", LOWER_LIMIT_EXAMPLE)
print("Upper limit: ", UPPER_LIMIT_EXAMPLE)
print("Number of intervals: ", NUM_INTERVALS_EXAMPLE)
print("Calculated integral: ", calculated_integral)
